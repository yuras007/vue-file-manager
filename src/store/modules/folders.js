import * as types from '../mutation-types'
import foldersArray from './folders.json'

const state = {
    folders: [],
    pinnedFolders: [],
    refsTree: null,
    refsPinnedTree: null,
    refsWorkPlace: null,
    currentFolder: {},
    currentItem: {},
    dialogFormVisible: false,
    itemsViewClass: false,
    action: null,
    searchField: ''
}

const getters = {
    folders: state => {
        return state.folders = foldersArray
    },
    pinnedFolders: state => {
        return state.pinnedFolders;
    },
    currentFolder: state => {
        return state.currentFolder;
    },
    currentItem: state => {
        return state.currentItem;
    },
    dialogFormVisible: state => {
        return state.dialogFormVisible;
    },
    itemsViewClass: state => {
        return state.itemsViewClass;
    },
    action: state => {
        return state.action;
    }
}

const actions = {

}

const mutations = {
    [types.SET_REFS_TREE] (state, ref) {
        state.refsTree = ref;
    },
    [types.SET_REFS_PINNED_TREE] (state, ref) {
        state.refsPinnedTree = ref;
    },
    [types.SET_REFS_WORK_PLACE] (state, ref) {
        state.refsWorkPlace = ref;
    },
    [types.UPDATE_CURRENT_FOLDER] (state, payload) {
        state.currentFolder = payload.items;
    },
    [types.ADD_TO_FAVORITE] (state, data) {
        state.refsPinnedTree.append(data.item);

        // console.log(state.pinnedItems)
        // state.pinnedItems.push(data.item);
        //TODO: state.pinnedItems updated but view not rerender
        // console.log('pinnedItems=',state.currentFolder);
    },
    [types.TOGGLE_ITEMS_VIEW] (state) {
        state.itemsViewClass = !state.itemsViewClass;
    },
    [types.UPDATE_CURRENT_ITEM] (state, payload) {
        state.currentItem = payload.item;
    },
    [types.RENAME_CURRENT_ITEM] (state, payload) {
        state.currentItem.label = payload.label;
    },
    [types.CREATE_FOLDER] (state, payload) {
        // function getObject(array, key, value) {
        //     var o;
        //     array.some(function iter(a) {
        //         if (a[key] === value) {
        //             o = a;
        //             return true;
        //         }
        //         return Array.isArray(a.children) && a.children.some(iter);
        //     });
        //     return o;
        // }

        state.refsTree.append(payload.item, state.currentFolder.id);

        // Vue.set(state.folders, 'children', state.folders.children);

        // let children = data || {};
        // state.currentFolder.children.push(payload.item);

        //TODO: alternative way
        // var folder = getObject(state.folders, 'id', state.currentFolder.id);
        // if (!folder.children) {
        //     folder.children = [];
        // }
        // folder.children.push(payload.item);

        // console.log('CurrenFolderData=',state.currentFolder);
    },
    [types.UPLOAD_FILE] (state, payload) {
        function getObject(array, key, value) {
            var o;
            array.some(function iter(a) {
                if (a[key] === value) {
                    o = a;
                    return true;
                }
                return Array.isArray(a.children) && a.children.some(iter);
            });
            return o;
        }

        var folder = getObject(state.folders, 'id', state.currentFolder.id);

        if (!folder.children) {
            folder.children = [];
        }

        state.currentFolder.children.push(payload.item);
        state.currentFolder = JSON.parse(JSON.stringify(state.currentFolder));

        //TODO: alternative way
        // folder.children.push(payload.item);
        // console.log('CurrenFolderData=',state.currentFolder);
    },
    [types.DELETE_ITEM] (state, payload) {
        function deleteItems(array, ids) {
            var i = array.length;
            while (i--) {
                if (ids.indexOf(array[i].id) !== -1) {
                    array.splice(i, 1);
                    continue;
                }
                array[i].children && deleteItems(array[i].children, ids);
            }
        }

        deleteItems(state.folders, [payload.item.id]);

        state.refsTree.remove(payload.item);
        state.folders = JSON.parse(JSON.stringify(state.folders));
        state.refsWorkPlace.remove(payload.item);
        state.currentFolder = JSON.parse(JSON.stringify(state.currentFolder));

        // state.folders = [];

        //TODO: alternative way
        // var index = state.currentFolder.children.indexOf(payload.item);
        // if (index > -1) {
        //     state.currentFolder.children.splice(index, 1);
        // }
        // console.log('CurrenFolderData=',state.currentFolder);
    },
    [types.TOGGLE_DIALOG_FORM] (state, payload) {
        state.dialogFormVisible = !state.dialogFormVisible;
        state.action = payload.action;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
