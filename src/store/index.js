import Vue from 'vue'
import Vuex from 'vuex'

import folders from './modules/folders'

Vue.use(Vuex)

export default  new Vuex.Store({
    modules: {
        folders
    }
})
